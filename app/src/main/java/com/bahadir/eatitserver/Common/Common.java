package com.bahadir.eatitserver.Common;

import com.bahadir.eatitserver.Model.Request;
import com.bahadir.eatitserver.Model.User;

public class Common {
    public static User currentUser;
    public static Request currentRequest;

    public  static  final String UPDATE ="Guncelle";
    public  static  final String DELETE = "Sil";


    public  static final int PICK_IMAGE_REQUEST =71;

    public static  String convertCodeToStatus(String code){
        if(code.equals("0"))
            return "Siparisler Beklemede";
        else if(code.equals("1"))
            return "Hazirlaniyor";
        else
            return "Hazirlandi";
    }

}
