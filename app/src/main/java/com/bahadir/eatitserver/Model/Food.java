package com.bahadir.eatitserver.Model;

public class Food {
    private String Name,Company,Image,Discount ,MenuId,Price,Description,Shelf,Barcode;

    public Food() {

    }

    public Food(String name, String company, String image, String discount, String menuId, String price, String description, String shelf,String barcode) {
        Name = name;
        Company = company;
        Image = image;
        Discount = discount;
        MenuId = menuId;
        Price = price;
        Description = description;
        Shelf = shelf;
        Barcode = barcode;
    }

    public String getBarcode() {
        return Barcode;
    }

    public void setBarcode(String barcode) {
        Barcode = barcode;
    }

    public String getShelf() {
        return Shelf;
    }

    public void setShelf(String shelf) {
        Shelf = shelf;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getMenuId() {
        return MenuId;
    }

    public void setMenuId(String menuId) {
        MenuId = menuId;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
