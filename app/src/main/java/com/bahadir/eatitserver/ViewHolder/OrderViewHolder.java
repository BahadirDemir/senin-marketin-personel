package com.bahadir.eatitserver.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bahadir.eatitserver.Interface.ItemClickListener;
import com.bahadir.eatitserver.R;

public class OrderViewHolder extends RecyclerView.ViewHolder{
    public TextView txtOrderId,txtOrderStatus,txtOrderPhone;

    public Button btnEdit , btnRemove , btnDetail , btnDirection ;


    public OrderViewHolder(@NonNull View itemView) {
        super(itemView);

        txtOrderId=(TextView)itemView.findViewById(R.id.order_id);
        txtOrderStatus=(TextView)itemView.findViewById(R.id.order_status);
        txtOrderPhone=(TextView)itemView.findViewById(R.id.order_phone);

        btnEdit=(Button)itemView.findViewById(R.id.btnEdit);
        btnDetail=(Button)itemView.findViewById(R.id.btnDetail);
        btnRemove=(Button)itemView.findViewById(R.id.btnRemove);
        btnDirection=(Button)itemView.findViewById(R.id.btnDirection);


    }
}
